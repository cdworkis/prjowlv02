%--- DATABASE dbCfg(string Key,string Val).

dbCfg("FileDir",		"/Users/charlesdworkis/Desktop/Projects/PrjOrmV01/").
dbCfg("FileProgram",	"/Users/charlesdworkis/Desktop/Projects/PrjOrmV01/CSysOrmV01.pl").
dbCfg("FileCfg",    	"/Users/charlesdworkis/Desktop/Projects/PrjOrmV01/Import/dbCfg.pl").
dbCfg("FileReq",		"/Users/charlesdworkis/Desktop/Projects/PrjOrmV01/Import/dbReq.pl").
dbCfg("FileRsp",		"/Users/charlesdworkis/Desktop/Projects/PrjOrmV01/Export/dbRsp.pl").
dbCfg("FileLog",		"/Users/charlesdworkis/Desktop/Projects/PrjOrmV01/Export/OrmToOwl_Log.txt").

dbCfg("FileOrmVdx1",	"/Users/charlesdworkis/Desktop/Projects/PrjOrmV01/Import/UB1-Transaction.vdx").
dbCfg("FileOrmImport0",	"/Users/charlesdworkis/Desktop/Projects/PrjOrmV01/Import/ORM_Test_01.vdx").
dbCfg("FileOrmExport0",	"/Users/charlesdworkis/Desktop/Projects/PrjOrmV01/Export/ORM_Test_01.pl").
dbCfg("FileOrmImport1",	"/Users/charlesdworkis/Desktop/Projects/PrjOrmV01/Import/UB1-Transaction.vdx").
dbCfg("FileOrmExport1",	"/Users/charlesdworkis/Desktop/Projects/PrjOrmV01/Export/UB1-Transaction.pl").
dbCfg("FileOrmImport2",	"/Users/charlesdworkis/Desktop/Projects/PrjOrmV01/Import/ATM.vdx").
dbCfg("FileOrmExport2",	"/Users/charlesdworkis/Desktop/Projects/PrjOrmV01/Export/ATM.pl").
dbCfg("FileOrmImport3",	"/Users/charlesdworkis/Desktop/Projects/PrjOrmV01/Import/UnParkACar.vdx").
dbCfg("FileOrmExport3",	"/Users/charlesdworkis/Desktop/Projects/PrjOrmV01/Export/UnParkACar.pl").
dbCfg("FileOrmImport4",	"/Users/charlesdworkis/Desktop/Projects/PrjOrmV01/Import/NextAngles_CarPark.vdx").
dbCfg("FileOrmExport4",	"/Users/charlesdworkis/Desktop/Projects/PrjOrmV01/Export/NextAngles_CarPark.pl").
dbCfg("FileOrmImport5",	"/Users/charlesdworkis/Desktop/Projects/PrjOrmV01/Import/ORM_Test_LX.vdx").
dbCfg("FileOrmExport5",	"/Users/charlesdworkis/Desktop/Projects/PrjOrmV01/Export/ORM_Test_LX.pl").
dbCfg("FileOrmImport6",	"/Users/charlesdworkis/Desktop/Projects/PrjOrmV01/Import/TestPartOfCarPark.vdx").
dbCfg("FileOrmExport6",	"/Users/charlesdworkis/Desktop/Projects/PrjOrmV01/Export/TestPartOfCarPark.pl").


%--- OWL Configuration - with temporary hard code for tests
dbCfg("FileShapeMX",	"/Users/charlesdworkis/Desktop/Projects/PrjOrmV01/Export/dbShapeMx.pl").
dbCfg("FileShapeCR",	"/Users/charlesdworkis/Desktop/Projects/PrjOrmV01/Export/dbShapeCr.pl").
dbCfg("FileShapeRel",	"/Users/charlesdworkis/Desktop/Projects/PrjOrmV01/Export/dbShapeRel.pl").
dbCfg("FileOrmToOwl",	"/Users/charlesdworkis/Desktop/Projects/PrjOrmV01/Export/OrmToOwl.ttl").
dbCfg("FileOwlExport1",	"/Users/charlesdworkis/Desktop/Projects/PrjOwlV02/Export/OrmToOwlRpt.ttl").
dbCfg("ExpOwlHdr_Base",	"# baseURI: http://nextangles.com/Tech/Workspace").
dbCfg("ExpOwlHdr_Imps",	"# imports: http://nextangles.com/Tech/Sets").
dbCfg("ExpOwlHdr_Imps",	"# imports: http://nextangles.com/Tech/metametamodel").
dbCfg("ExpOwl_Prefix",	" ").
dbCfg("ExpOwl_Prefix",	"@prefix owl: <http://www.w3.org/2002/07/owl#> .").
dbCfg("ExpOwl_Prefix",	"@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .").
dbCfg("ExpOwl_Prefix",	"@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .").
dbCfg("ExpOwl_Prefix",	"@prefix set: <http://nextangles.com/Tech/Sets#> .").
dbCfg("ExpOwl_Prefix",	"@prefix ws: <http://nextangles.com/Tech/Workspace#> .").
dbCfg("ExpOwl_Prefix",	"@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .").
dbCfg("ExpOwl_Prefix",	" ").
dbCfg("ExpOwl_ClsDef", "<owl:Class rdf:ID='|=CLASSNAME=|'>\r\n    <mmm:modeledAs rdf:resource='http://nextangles.com/Tech/metametamodel#|=METAMODELCLASS=|'/>\r\n    <rdfs:subClassOf>\r\n      <owl:NamedIndividual rdf:about='http://www.w3.org/2002/07/owl#Thing'/>\r\n    </rdfs:subClassOf>\r\n    <rdfs:label rdf:datatype='http://www.w3.org/2001/XMLSchema#string'\r\n    >|=CLASSLABEL=|</rdfs:label>\r\n  </owl:Class>\r\n\r\n").

%--- OWL Configuration - generate OWL in XML or FNS syntax (functional spec)
dbCfg("ExpOwl_Syntax",	"FNS").
