% PROG:  ["/Users/charlesdworkis/Desktop/Projects/PrjKB1TheaV01/CSysKB1TheaV01.pl"].

/*
	% =====================================================================================
	To run on Mac:
		1.  Open Terminal
		2.  At Terminal Prompt enter: Swipl  then tap enter; note: no period!
		3.  At Terminal, verify Swi Prolog presents: Welcome to SWI-Prolog (Multi-threaded, 64 bits, Version 7.2.3)
		4.  At Terminal Prompt load this program by entering: ["/Users/charlesdworkis/Desktop/Projects/PrjKB1TheaV01/CSysKB1TheaV01.pl"].
		5.  If properly loaded SWI Prolog will report "true" and present interactive prompt. Otherwise correct any reported issues. 
		6.  Enter:  tx().   to launch...
		7.  Review results in: /Users/charlesdworkis/Desktop/Projects/PrjKB1TheaV01/Export
		8.  Enter:  gen().  to list tests...
		9.  Enter: listing(dbCfg). Verify that /import/dbCfg.pl properly configured your run.
		10. Review results in: /Users/charlesdworkis/Desktop/Projects/PrjKB1TheaV01/Export/...
		11. Optionally trace:   trace.  ...  notrace.
	% =====================================================================================
*/

	% LIBRARIES ===========================================================================
    % DBUGTODO Following hardcoded file locations are test only. Make them configurable via run directory
	:- use_module(library(pio)).
	:- ensure_loaded(
	  		[	"/Users/charlesdworkis/Desktop/Projects/PrjKB1TheaV01/Lib/CSysStrV02.pl",
	  			"/Users/charlesdworkis/Desktop/Projects/PrjKB1TheaV01/Lib/CSysListV02.pl",
	  			"/Users/charlesdworkis/Desktop/Projects/PrjKB1TheaV01/Lib/CSysFileV02.pl",
	  			"/Users/charlesdworkis/Desktop/Projects/PrjKB1TheaV01/Lib/CSysDictV01.pl",
	  			"/Users/charlesdworkis/Desktop/Projects/PrjKB1TheaV01/Lib/CSysKBV01.pl",
	  			"/Users/charlesdworkis/Desktop/Projects/PrjKB1TheaV01/Lib/CSysGrammarV01.pl",

	  			"/Users/charlesdworkis/Desktop/Projects/PrjKB1TheaV01/Import/dbCfg.pl"
			]).
	:- dynamic dbDict/3.
	
 
	% DB ==================================================================================	
	%Pred: dbInit() initialize DB
	%Decl: dbInit : () determ().
    %E.G.: dbInit()
	dbInit()
	:-	%retractall(dbShapeLoc(_,_,_,_,_,_)),
		post("Launching tx().........","dbInit() retracted ..."),
		post("Launching tx().........","dbInit() loaded dbCfg"),
		post("Launching tx().........","dbInit() loaded ..."),
		post("Launching tx().........","dbInit() OK"),
		!.
		
	% EXECS ===============================================================================
	%Pred: tx() translate KB1 Model to OWL 
	%Decl: tx : () determ().
    %E.G.: tx()
	tx()
	:-  gline(Line),
		strDateTime(SDateTime),
		post(Line,""),
		post("Launching tx().........",SDateTime),
		dbInit(),
		post("Launching tx().........","Ready"),
		
		%parse KB OWL into RDF TTL & export result to configured FileOwlRDFTTLExport1
		post("Launching tx().........",	"Processing..."),
   		process("KB->RDFOWL","SyntaxTarget",SRpt),
		post("Launching tx().........",	"Processed"),
		post(Line,""),
		post(SRpt),
		post(Line,""),
		post("Launching tx().........",	"Exporting..."),
		%dbCfg("FileOwlExport1",FileOwlExport1),
		%fileWriteText(FileOwlExport1,SRpt),
		post("Launching tx().........",	"Exported"),
		post(Line,""),
		!.
	tx()
	:-  post("Launching tx().........","tx() failed"),
		!.
	
   	% PARSER ==============================================================================
	
   	%Pred: process(string GrammarID,string SyntaxTarget,string SText) - parse SText per specified grammar & syntax
   	%Decl: process : (string GrammarID,string SyntaxTarget,string SText) determ(i,i,o).
	%E.G.: process("KB->RDFOWL",Syntax,SRpt)
	%Note: 
   	process("KB->RDFOWL",SyntaxTarget,"Process Results>") 
   	:- ontology("OntologyAlpha").

   	% ONTOLOGY ============================================================================
	
   	%Pred: ontology(string Id) - define an ontology
   	%Decl: ontology : (string OntologyId) determ(i).
	%E.G.: ontology("OntologyAlpha")
   	ontology(OntologyId,axioms(AxiomList)) 
   	:- 	post("Creating ontology......",	OntologyId),
		axiomList(AxiomList),
		entityList(EntityList),
		postx("Axioms.................",AxiomList,"\r\n"),
		%post("Entities...............",EntityList),
		!.
		


		axiomList(Xs):- findall(X,axiom(X),Xs).
		axiom(A):- declaration(A).
		axiom(A):- classAxiom(A).
		axiom(A):- propertyAxiom(A).
		axiom(A):- objectPropertyAxiom(A).
		axiom(A):- dataPropertyAxiom(A).
		axiom(A):- hasKey(A).
		axiom(A):- assertion(A).
		axiom(A):- annotationAxiom(A).



declaration(declaration(axiomAnnotations(A),entity(E))).

classAxiom(subClassOf(Annotation,CSub,CPar)).
classAxiom(equivalentClasses(CX,CY)).
classAxiom(disjointClasses(CX,CY)).
classAxiom(disjointUnion(CX,CY)).

propertyAxiom(A).

property(A):- dataProperty(A).
property(A):- objectProperty(A).


objectPropertyAxiom(A):- tbd(A).
dataPropertyAxiom(A):- tbd(A).
hasKey(A):- tbd(A).
assertion(A):- tbd(A).
annotationAxiom(A):- tbd(A).

axiomAnnotations(annotation(X)).

		entityList(Xs):- findall(X,entity(X),Xs).
		entity(class(X)).
		entity(dataType(X)).
		entity(objectProperty(X)).
		entity(dataProperty(X)).
		entity(annotationProperty(X)).
		entity(namedIndividual(X)).




			subClassOf(classExpression(SubClass), classExpression(SuperClass)):- diff(SubClass,SuperClass).
			equivalentClasses([CX|CT]).
			setOfClasses(ClassList1,ClassList2).
disjointClasses(CX,CY).
disjointUnion(CX,CY).

propertyChain(propertyList(PX)).
subPropertyOf(propertyChain(PL,property(PX))).
subPropertiesOf([subPropertyOf(SP)|T]).


classExpression(class(CX)).
classExpression(objectIntersectionOf(CX,CY)).
classExpression(objectTBDOf(CX,CY)).

			inverseProperties(property(P1),property(P2)).

objectIntersectionOf(classExpression(X),classExpression(Y)).


classExpressionList([X|T]):- classExpression(X), !, classExpressionList(T).


%axiomList([]).
%axiomList([axiom(A)|T]):- axiom(A), !, axiomList(T).


%-------------------------------------
annotation("Annotation_CarPark").
class("Class_Car").
class("Class_Vehicle").

tbd(X).





	% UTILITY =============================================================================

	%Pred: post(string Text1,string Text2) display & log specified text
	%Decl: post : (string Text1,string Text2) determ(i,i).
	%E.G.: post("Tag","Details...")

	post(Text1,Text2)
	:- 	format('~s~t~s~n', [Text1,Text2]),
		log(Text1,Text2).
	post(Text)
	:- 	format('~s~n', [Text]).
	post([]).
	post([H|T]):- post(H), !, post(T).
	
	postx(Prefix,L,Suffix):- write(Prefix),write(L),write(Suffix).
	
	%Pred: log(string Text1,string Text2) append specified text to log
	%Decl: log : (string Text1,string Text2) determ(i,i).
	%E.G.: log("Tag","Details...")
	log(Text1,Text2)
	:- format(atom(LogRcd),'~s~t~8|~s', [Text1,Text2]),
		dbCfg("FileLog",File),
		fileAppend(File,LogRcd), 
		!.

	%Pred: pause(string Msg) print message and wait for any single key stroke from user
	%Decl: pause : (string Msg) determ(i).
	%E.G.: pause("Press any key to continue...")
	pause(Msg)
	:- 	print_message(informational,Msg),
		get_single_char(_).


	predicate(PredArgs,Predicate,Args):- compound_name_arguments(PredArgs,Predicate,Args).
	%clause(:Head, ?Body)
	%functor(?Term, ?Name, ?Arity)
	%arg(?Arg, +Term, ?Value)
	%?Term =.. ?List
	%foo(hello, X) =.. List.
	%Term =.. [baz, foo(1)].
	
	%convert [Functor|Args] into functor(args)
	predicateFromParts([Functor|Args],Predicate):- Predicate =.. [Functor|Args].
	
	%convert functor(args) into [Functor|Args] 
	predicateIntoParts(Predicate,[Functor|Args]):- Predicate =.. [Functor|Args].
	
	
	diff(X,X):- !, fail.
	diff(_,_):- !.

	list([])     --> [].
	list([L|Ls]) --> [L], list(Ls).

	% EXPORTS =============================================================================
	% TEST ================================================================================	
	% TEMP ================================================================================
	% TO DO ===============================================================================
	% SCRAP ===============================================================================
				%post(Text1,[List])
				%:- 	strFromlist([List]," & ",Text2),
				%	format('~s~t~s~n', [Text1,Text2]),
				%	log(Text1,List).
		
	% EOF =================================================================================



